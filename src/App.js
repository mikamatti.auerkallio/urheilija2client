import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { Provider } from "./context";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Header from "./components/Header";
import Urheilijat from "./components/Urheilijat";
import Lisaaurheilija from "./components/Lisaaurheilija";
import Muokkaaurheilija from "./components/Muokkaaurheilija";

function App() {
  return (
    <Provider>
      <Router>
        <div className="App">
          {/*Header*/}
          <Header title="URHEILUSIVU" />
          {/*Main*/}
          <div className="container">
            <Switch>
              <Route exact path="/" component={Urheilijat} />
              <Route exact path="/urheilija/lisaa" component={Lisaaurheilija} />
              <Route
                exact
                path="/urheilija/muokkaa/:id"
                component={Muokkaaurheilija}
              />
            </Switch>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
