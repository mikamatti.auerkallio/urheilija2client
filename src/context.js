import React, { Component } from "react";
import axios from "axios";

const Context = React.createContext();

//Switchcase for reducer
const reducer = (state, action) => {
  switch (action.type) {
    case "ADD_URHEILIJA":
      return {
        ...state,
        urheilijat: [action.payload, ...state.urheilijat],
      };
    case "UPDATE_URHEILIJA":
      return {
        ...state,
        urheilijat: state.urheilijat.map((urheilija) =>
          urheilija.id === action.payload.id
            ? (urheilija = action.payload)
            : urheilija
        ),
      };
    case "DELETE_URHEILIJA":
      return {
        ...state,
        urheilijat: state.urheilijat.filter(
          (urheilija) => urheilija.id !== action.payload
        ),
      };
    default:
      return state;
  }
};

export class Provider extends Component {
  state = {
    urheilijat: [],
    //Dispatch method
    dispatch: (action) => this.setState((state) => reducer(state, action)),
  };

  componentDidMount() {
    axios
      .get("http://localhost:8080/urheilijat")
      .then((res) => this.setState({ urheilijat: res.data }));
  }

  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

export const Consumer = Context.Consumer;
