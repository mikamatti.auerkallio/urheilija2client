import React, { Component } from "react";
import { Consumer } from "../context";
import { Container, Row, Col } from "reactstrap";
import axios from "axios";

export default class Muokkaaurheilija extends Component {
  state = {
    etunimi: "",
    sukunimi: "",
    kutsumanimi: "",
    syntymavuosi: "",
    paino: "",
    kuvalinkki: "",
    laji: "",
    saavutukset: "",
  };

  onSubmit = (dispatch, e) => {
    e.preventDefault();
    //Unpack this.state to variables
    const {
      etunimi,
      sukunimi,
      kutsumanimi,
      syntymavuosi,
      paino,
      kuvalinkki,
      laji,
      saavutukset,
    } = this.state;

    const id = Number(this.props.match.params.id);

    const newUrheilija = {
      id,
      etunimi,
      sukunimi,
      kutsumanimi,
      syntymavuosi,
      paino,
      kuvalinkki,
      laji,
      saavutukset,
    };

    console.log("Lähetettävät tiedot", newUrheilija);

    axios
      .put(`http://localhost:8080/urheilijat/${id}`, newUrheilija)
      .then((res) => {
        dispatch({ type: "UPDATE_URHEILIJA", payload: res.data });
        console.log(res.data);
      });

    this.props.history.push("/");
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    console.log("id ", id);
    //Get old information to fill the forms
    axios.get(`http://localhost:8080/urheilijat/${id}`).then((res) => {
      const urheilijatiedot = res.data[0];

      const parseDob = urheilijatiedot.syntymavuosi.split(/[- T:]/);
      const formattedSyntymavuosi =
        parseDob[0] + "-" + parseDob[1] + "-" + parseDob[2];

      this.setState({
        etunimi: urheilijatiedot.etunimi,
        sukunimi: urheilijatiedot.sukunimi,
        kutsumanimi: urheilijatiedot.kutsumanimi,
        syntymavuosi: formattedSyntymavuosi,
        paino: urheilijatiedot.paino,
        kuvalinkki: urheilijatiedot.kuvalinkki,
        laji: urheilijatiedot.laji,
        saavutukset: urheilijatiedot.saavutukset,
      });

      //console.log("State", this.state);
    });
  }

  render() {
    const {
      etunimi,
      sukunimi,
      kutsumanimi,
      syntymavuosi,
      paino,
      kuvalinkki,
      laji,
      saavutukset,
    } = this.state;

    return (
      <Consumer>
        {(value) => {
          const { dispatch } = value;
          console.log(dispatch);
          return (
            <div>
              <div className="card text-left my-3">
                <div className="card-header">Muokkaa urheilija</div>
                <div className="card-body">
                  <form onSubmit={this.onSubmit.bind(this, dispatch)}>
                    <Container>
                      <Row>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Etunimi</label>
                            <input
                              type="text"
                              name="etunimi"
                              className="form-control"
                              placeholder="Syötä etunimi..."
                              value={etunimi}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Sukunimi</label>
                            <input
                              type="text"
                              name="sukunimi"
                              className="form-control"
                              placeholder="Syötä sukunimi..."
                              value={sukunimi}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Kutsumanimi</label>
                            <input
                              type="text"
                              name="kutsumanimi"
                              className="form-control"
                              placeholder="Syötä kutsumanimi..."
                              value={kutsumanimi}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Syntymäaika</label>
                            <input
                              type="date"
                              name="syntymavuosi"
                              className="form-control"
                              placeholder="Valitse syntymävuosi..."
                              value={syntymavuosi}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Paino</label>
                            <input
                              type="number"
                              min="30"
                              max="400"
                              name="paino"
                              className="form-control"
                              placeholder="Syötä paino kiloina..."
                              value={paino}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Laji</label>
                            <input
                              type="text"
                              name="laji"
                              className="form-control"
                              placeholder="Syötä laji..."
                              value={laji}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Kuvalinkki</label>
                            <input
                              type="text"
                              name="kuvalinkki"
                              className="form-control"
                              placeholder="Syötä kuvalinkki..."
                              value={kuvalinkki}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Saavutukset</label>
                            <textarea
                              type="text"
                              name="saavutukset"
                              className="form-control form-control"
                              placeholder="Luettele saavutukset..."
                              value={saavutukset}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                      </Row>
                    </Container>

                    <input
                      type="submit"
                      value="Muokkaa urheilija"
                      className="btn btn-secondary btn-block"
                    />
                  </form>
                </div>
              </div>
            </div>
          );
        }}
      </Consumer>
    );
  }
}
