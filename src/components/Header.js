import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Header = (props) => {
  const { title } = props;

  return (
    <nav className="navbar navbar-expand-md navbar-dark bg-dark">
      <Link to="/" className="navbar-brand">
        {title}
      </Link>
      <div className="navbar-collapse collapse justify-content-stretch">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link to="/urheilija/lisaa" className="nav-link text-light">
              Lisää urheilija
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/" className="nav-link text-light">
              Alkuun
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

Header.defaultProps = {
  title: "Title not found",
};

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Header;
