import React, { Component } from "react";
import { Consumer } from "../context";
import axios from "axios";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default class Urheilijatieto extends Component {
  onDeleteClick = (id, dispatch) => {
    axios
      .delete(`http://localhost:8080/urheilijat/${id}`)
      .then((res) => dispatch({ type: "DELETE_URHEILIJA", payload: id }));
  };

  render() {
    const {
      id,
      etunimi,
      sukunimi,
      kutsumanimi,
      syntymavuosi,
      paino,
      kuvalinkki,
      laji,
      saavutukset,
    } = this.props.urheilija;

    return (
      <Consumer>
        {(value) => {
          const { dispatch } = value;
          return (
            <div className="card mx-3 mb-3" style={{ width: 300 }}>
              <div className="btn-group  rounded-top" role="group">
                <Link
                  type="button"
                  className="btn btn-info rounded-0 w-50"
                  to={`urheilija/muokkaa/${id}`}
                >
                  Muokkaa
                </Link>
                <button
                  type="button"
                  className="btn btn-secondary rounded-0 w-50"
                  onClick={this.onDeleteClick.bind(this, id, dispatch)}
                >
                  Poista
                </button>
              </div>

              <img
                src={kuvalinkki}
                className="card-img rounded-0"
                alt="Card cap"
                style={{ height: 300, objectFit: "cover" }}
              ></img>

              <div className="card-body">
                <h4 className="card-title">
                  {etunimi} {sukunimi}
                </h4>
                <ul className="list-group text-left">
                  <li className="list-group-item">
                    <b>Laji:</b> {laji}
                  </li>
                  <li className="list-group-item">
                    <b>Kutsumanimi:</b> {kutsumanimi}
                  </li>

                  <li className="list-group-item">
                    <b>Syntymävuosi:</b> {parseInt(syntymavuosi)}
                  </li>
                  <li className="list-group-item">
                    <b>Paino</b>: {paino} kg
                  </li>
                  <li className="list-group-item">
                    <b>Saavutukset</b>: {saavutukset}
                  </li>
                </ul>
              </div>
            </div>
          );
        }}
      </Consumer>
    );
  }
}

Urheilijatieto.propTypes = {
  urheilija: PropTypes.object.isRequired,
};
