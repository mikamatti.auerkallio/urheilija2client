import React, { Component } from "react";
import { Consumer } from "../context";
import { Container, Row } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";

import Urheilijatieto from "./Urheilijatieto";

export default class Urheilijat extends Component {
  constructor() {
    super();
    this.state = {
      urheilijat: "",
    };
  }

  render() {
    return (
      <Consumer>
        {(value) => {
          const { urheilijat } = value;
          return (
            <div>
              <h1 className="display-5 py-3">
                <span className="text-info">URHEILIJAT</span>
              </h1>
              <Container className="d-flex justify-content-center">
                <Row>
                  {urheilijat.map((urheilijatieto) => (
                    <Urheilijatieto
                      key={urheilijatieto.id}
                      urheilija={urheilijatieto}
                    />
                  ))}
                </Row>
              </Container>
            </div>
          );
        }}
      </Consumer>
    );
  }
}
