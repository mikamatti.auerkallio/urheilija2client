import React, { Component } from "react";
import { Consumer } from "../context";
import { Container, Row, Col } from "reactstrap";
import axios from "axios";

export default class LisaaUrheilija extends Component {
  state = {
    etunimi: "",
    sukunimi: "",
    kutsumanimi: "",
    syntymavuosi: "",
    paino: "",
    kuvalinkki: "",
    laji: "",
    saavutukset: "",
  };

  onSubmit = (dispatch, e) => {
    e.preventDefault();
    console.log(this.state);
    //Unpack this.state to variables
    const {
      etunimi,
      sukunimi,
      kutsumanimi,
      syntymavuosi,
      paino,
      kuvalinkki,
      laji,
      saavutukset,
    } = this.state;

    const newUrheilija = {
      etunimi: etunimi,
      sukunimi: sukunimi,
      kutsumanimi: kutsumanimi,
      syntymavuosi: syntymavuosi,
      paino: paino,
      kuvalinkki: kuvalinkki,
      laji: laji,
      saavutukset: saavutukset,
    };

    axios.post(`http://localhost:8080/urheilijat`, newUrheilija).then((res) => {
      dispatch({ type: "ADD_URHEILIJA", payload: res.data });
      console.log(res.data);
    });

    this.props.history.push("/");
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const {
      etunimi,
      sukunimi,
      kutsumanimi,
      syntymavuosi,
      paino,
      kuvalinkki,
      laji,
      saavutukset,
    } = this.state;

    return (
      <Consumer>
        {(value) => {
          const { dispatch } = value;
          return (
            <div>
              <div className="card text-left my-3">
                <div className="card-header">Lisää urheilija</div>
                <div className="card-body">
                  <form onSubmit={this.onSubmit.bind(this, dispatch)}>
                    <Container>
                      <Row>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Etunimi</label>
                            <input
                              type="text"
                              name="etunimi"
                              className="form-control"
                              placeholder="Syötä etunimi..."
                              value={etunimi}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Sukunimi</label>
                            <input
                              type="text"
                              name="sukunimi"
                              className="form-control"
                              placeholder="Syötä sukunimi..."
                              value={sukunimi}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Kutsumanimi</label>
                            <input
                              type="text"
                              name="kutsumanimi"
                              className="form-control"
                              placeholder="Syötä kutsumanimi..."
                              value={kutsumanimi}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Syntymäaika</label>
                            <input
                              type="date"
                              name="syntymavuosi"
                              className="form-control"
                              placeholder="Valitse syntymävuosi..."
                              value={syntymavuosi}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Paino</label>
                            <input
                              type="number"
                              min="30"
                              max="400"
                              name="paino"
                              className="form-control"
                              placeholder="Syötä paino kiloina..."
                              value={paino}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Laji</label>
                            <input
                              type="text"
                              name="laji"
                              className="form-control"
                              placeholder="Syötä laji..."
                              value={laji}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Kuvalinkki</label>
                            <input
                              type="text"
                              name="kuvalinkki"
                              className="form-control"
                              placeholder="Syötä kuvalinkki..."
                              value={kuvalinkki}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <div className="form-group">
                            <label htmlFor="name">Saavutukset</label>
                            <textarea
                              type="text"
                              name="saavutukset"
                              className="form-control form-control"
                              placeholder="Luettele saavutukset..."
                              value={saavutukset}
                              onChange={this.onChange}
                              required
                            />
                          </div>
                        </Col>
                      </Row>
                    </Container>

                    <input
                      type="submit"
                      value="Lisää urheilija"
                      className="btn btn-secondary btn-block"
                    />
                  </form>
                </div>
              </div>
            </div>
          );
        }}
      </Consumer>
    );
  }
}
